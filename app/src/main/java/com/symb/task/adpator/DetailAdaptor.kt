package com.symb.task.adpator

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import com.symb.task.Country
import com.symb.task.CountryAPI
import com.symb.task.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.internal.schedulers.IoScheduler
import kotlinx.android.synthetic.main.detail_layout.view.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class DetailViewAdaptor(var context: Context, var CountryList: List<Country>) :
    RecyclerView.Adapter<DetailViewAdaptor.ViewHolder>() {
    var countries: MutableList<Country> = ArrayList()
    var displayList: MutableList<Country> = ArrayList()

    private val retrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl("https://restcountries.eu/rest/v2/").build()
    private val countryApi: CountryAPI = retrofit.create(CountryAPI::class.java)
    private val response: Observable<List<Country>> = countryApi.getAllPosts()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.detail_layout,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return CountryList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = "Name : " + CountryList[position].name
        holder.capital.text = "Capital : " + CountryList[position].capital
        holder.alpha2Code.text = "Alpha2Code : " + CountryList[position].alpha2Code
        holder.population.text = "Population : " + CountryList[position].population.toString()

        response.observeOn(AndroidSchedulers.mainThread()).subscribeOn(IoScheduler()).subscribe {
            val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true)
            holder.borderRecycle.layoutManager = layoutManager
            holder.borderRecycle.adapter = BorderAdapter(context, displayList)
            fun addData() {
                for (item in it) {
                    countries.add(item)
                }
            }
            addData()
            val border = CountryList[position].borders
            if (border.isNotEmpty()) {
                for (item in border) {
                    val name = jp.gr.java_conf.androtaku.countrylist.CountryList.convertCodeToName(
                        context,
                        item.dropLast(1)
                    )
                    countries.forEach {
                        if (it.name.contains(name.toString()))
                            displayList.add(it)
                    }
                }
            }
        }

        val bordersList = CountryList[position].borders
        if (bordersList.isEmpty()) {
            holder.borders.text = "Borders Not Available "
        } else {
            var borders = ""
            borders += "\n"
            for (item in bordersList) {
                val borderName =
                    jp.gr.java_conf.androtaku.countrylist.CountryList.convertCodeToName(
                        context,
                        item.dropLast(1)
                    )
                if (borderName != null)
                    borders = "$borders$borderName,  "
                Toast.makeText(context, borderName, Toast.LENGTH_SHORT).show()
            }
            holder.borders.text = borders.trim()
        }


        val callingCodesList = CountryList[position].callingCodes
        if (callingCodesList.isEmpty()) {
            holder.callingCodes.text = " Calling Codes Not Available"
        } else {
            for (item in callingCodesList) {
                holder.callingCodes.text = "Calling Code  - $item"
            }
        }

        val timezonesList = CountryList[position].timezones
        if (timezonesList.isEmpty()) {
            holder.timezones.text = "Timezone Not Available "
        } else {
            for (item in timezonesList)
                holder.timezones.text = "TimeZone  - $item"
        }

        val languageList = CountryList[position].languages
        var name = ""
        var countLanguage = 1
        for (item in languageList) {
            name = "$name${item.name}, "
            countLanguage++
        }
        holder.lang_name.text = name.trim()

        val currencyList = CountryList[position].currencies
        for (item in currencyList)
            holder.currency.text = "Currency  - " + item.name

        Picasso.get().load(
            "https://www.countryflags.io/" + CountryList[position].alpha2Code.toLowerCase(Locale.ROOT) + "/flat/64.png"
        ).into(holder.image)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var borderRecycle: RecyclerView = view.border_view
        var name: TextView = view.countryName
        var capital: TextView = view.countryCapital
        var alpha2Code: TextView = view.countryAlphaCode
        var image: ImageView = view.flagImageView
        var population: TextView = view.countryPopulation
        var borders: TextView = view.countryBorders
        var callingCodes: TextView = view.countryCode
        var lang_name: TextView = view.countryLanguage
        var timezones: TextView = view.countryTimeZone
        var currency: TextView = view.countryCurrency
    }
}
