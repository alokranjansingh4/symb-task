package com.symb.task.adpator

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.symb.task.Country
import com.symb.task.R
import com.symb.task.SecondMainActivity
import kotlinx.android.synthetic.main.item_list.view.*
import java.util.*

class CountryNameAdapter(val context: Context, var CountryList: List<Country>) :
    RecyclerView.Adapter<CountryNameAdapter.CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(context).inflate(
            R.layout.item_list,
            parent, false
        )
        return CustomViewHolder(layoutInflater)
    }

    override fun getItemCount(): Int {
        return CountryList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val name = CountryList[position].name
        val zone = CountryList[position].timezones.toString()
        val al2code = CountryList[position].alpha2Code
        holder.view.name?.text = CountryList[position].name
        holder.view.capital?.text = "Capital - " + CountryList[position].capital

        holder.view.countryTime.setOnClickListener {
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.time_dialog, null)
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)

            val dialogCN = mDialogView.findViewById<TextView>(R.id.dialog_country_name)
            dialogCN.text = name

            val dialogImg = mDialogView.findViewById<ImageView>(R.id.dialog_image)
            Picasso.get().load(
                "https://www.countryflags.io/" + al2code.toLowerCase(
                    Locale.ROOT
                ) + "/flat/64.png"
            ).into(dialogImg)

            val tz = TimeZone.getTimeZone(zone)
            val c = Calendar.getInstance(tz)
            val time = ": " + String.format(
                "%02d",
                c.get(Calendar.HOUR_OF_DAY)
            ) + ":" + String.format("%02d", c.get(Calendar.MINUTE))

            Toast.makeText(context, zone, Toast.LENGTH_SHORT).show()

            val dialogZone = mDialogView.findViewById<TextView>(R.id.dialog_current_time)
            dialogZone.text = time
            val mAlertDialog = mBuilder.show()

            val dialogBtn = mDialogView.findViewById<Button>(R.id.dialog_ok)
            dialogBtn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }

        val thumbnailImageView = holder.view.flag_imageView
        Picasso.get().load(
            "https://www.countryflags.io/" + al2code.toLowerCase(
                Locale.ROOT
            ) + "/flat/64.png"
        ).into(thumbnailImageView)
        holder.name = name
    }

    class CustomViewHolder(var view: View, var name: String? = null) :
        RecyclerView.ViewHolder(view) {
        companion object {
            var country_name = "name"
        }

        init {
            view.setOnClickListener {
                val intent = Intent(view.context, SecondMainActivity::class.java).apply {
                    putExtra(country_name, name)
                }
                view.context.startActivity(intent)
            }
        }
    }
}

