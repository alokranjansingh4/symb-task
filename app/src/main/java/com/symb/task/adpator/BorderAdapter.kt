package com.symb.task.adpator

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.symb.task.Country
import com.symb.task.R
import com.symb.task.SecondMainActivity
import kotlinx.android.synthetic.main.item_list2.view.*
import java.util.*

class BorderAdapter(val context: Context, var CountryList: List<Country>) :
    RecyclerView.Adapter<BorderAdapter.CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(context).inflate(
            R.layout.item_list2,
            parent, false
        )
        return CustomViewHolder(layoutInflater)
    }

    override fun getItemCount(): Int {
        return CountryList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val name = CountryList[position].name
        holder.view.nCountry?.text = CountryList[position].name

        val thumbnailImageView = holder.view.flag_image
        Picasso.get().load(
            "https://www.countryflags.io/" + CountryList[position].alpha2Code.toLowerCase(Locale.ROOT) + "/flat/64.png"
        ).into(thumbnailImageView)
        holder.name = name
    }

    class CustomViewHolder(var view: View, var name: String? = null) :
        RecyclerView.ViewHolder(view) {
        companion object {
            var country_name = "name"
        }

        init {
            view.setOnClickListener {
                val intent = Intent(view.context, SecondMainActivity::class.java).apply {
                    putExtra(country_name, name)
                }
                view.context.startActivity(intent)
            }
        }
    }
}
