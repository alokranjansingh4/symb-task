package com.symb.task

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.GsonBuilder
import com.symb.task.adpator.CountryNameAdapter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.internal.schedulers.IoScheduler
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    var mAuth: FirebaseAuth? = null
    var mUser: FirebaseUser? = null
    var countries: MutableList<Country> = ArrayList()
    var displayList: MutableList<Country> = ArrayList()

    private lateinit var layoutManager: RecyclerView.LayoutManager

    @SuppressLint("CheckResult", "ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(resources.getColor(android.R.color.white))

//        sign_out_btn.setOnClickListener {
//            startActivity(Intent(applicationContext, LoginActivity::class.java))
//            FirebaseAuth.getInstance().signOut()
//        }

        FirebaseApp.initializeApp(this)
        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                override fun onSuccess(instanceIdResult: InstanceIdResult) {
                    val token = instanceIdResult.token //Token
                    Log.e("notification", "Refreshed token: " + token)
                }
            })

        mAuth = FirebaseAuth.getInstance()
        mUser = mAuth!!.currentUser
        if (mUser == null)
            startActivity(Intent(applicationContext, LoginActivity::class.java))

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(OkHttpClient.Builder().build())
            .baseUrl("https://restcountries.eu/rest/v2/").build()

        val countryApi = retrofit.create(CountryAPI::class.java)
        val response: Observable<List<Country>> = countryApi.getAllPosts()


        mainProgress.visibility = View.VISIBLE
        response.observeOn(AndroidSchedulers.mainThread()).subscribeOn(IoScheduler()).subscribe {
            layoutManager = LinearLayoutManager(this)
            rv__list_name.layoutManager = layoutManager
            rv__list_name.adapter = CountryNameAdapter(this, it)

            fun addData() {
                for (item in it) {
                    countries.add(item)
                }
            }
            addData()
            mainProgress.visibility = View.INVISIBLE
        }
    }

    private fun disconnected() {
        rv__list_name.visibility = View.INVISIBLE
        imageView.visibility = View.VISIBLE
    }

    private fun connected() {
        rv__list_name.visibility = View.VISIBLE
        imageView.visibility = View.INVISIBLE
    }

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager.EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
    }


    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        val searchItem = menu.findItem(R.id.menu_search)
        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView
            val edited = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            edited.hint = "Search"

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    displayList.clear()
                    if (newText!!.isNotEmpty()) {
                        val search = newText.toLowerCase(Locale.ROOT)
                        countries.forEach {
                            if (it.name.toLowerCase(Locale.ROOT)
                                    .contains(search) || it.capital.toLowerCase(
                                    Locale.ROOT
                                ).contains(search)
                            ) {
                                displayList.add(it)
                            }
                        }
                    } else {
                        displayList.addAll(countries)
                    }
                    rv__list_name.adapter = CountryNameAdapter(baseContext, displayList)
                    rv__list_name.adapter?.notifyDataSetChanged()
                    return true
                }
            })
        }
        return super.onCreateOptionsMenu(menu)
    }
}
