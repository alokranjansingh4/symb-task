package com.symb.task

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.symb.task.adpator.CountryNameAdapter
import com.symb.task.adpator.DetailViewAdaptor
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_second_main.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class SecondMainActivity : AppCompatActivity() {
    private lateinit var layoutManager: RecyclerView.LayoutManager

    @SuppressLint("CheckResult", "ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_main)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(resources.getColor(android.R.color.white))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val fullName = intent.getStringExtra(CountryNameAdapter.CustomViewHolder.country_name)

        supportActionBar?.title = fullName

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://restcountries.eu/rest/v2/name/").build()

        val fullNameApi = retrofit.create(CountryAPIDetail::class.java)
        val response: Observable<List<Country>> = fullNameApi.getAllCountryByName(fullName, "true")

        secondProgress.visibility = View.VISIBLE
        response.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
            layoutManager = LinearLayoutManager(this)
            detail_view.layoutManager = layoutManager
            detail_view.adapter = DetailViewAdaptor(this, it)
            secondProgress.visibility = View.INVISIBLE
        }
    }
}
