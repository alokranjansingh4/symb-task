package com.symb.task.service

import android.R
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.symb.task.MainActivity
import java.io.IOException
import java.net.URL


class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val notification = remoteMessage.notification
        val data = remoteMessage.data
//        Log.e("notification", "" + notification!!.body)
//        Log.e("notification", "" + notification.imageUrl)
//        Log.e("notification", "" + notification.body)
        if (notification != null) {
            showNotification(notification, data)
        }

    }

    private fun showNotification(
        notification: RemoteMessage.Notification,
        data: Map<String, String>
    ) {
        val icon = BitmapFactory.decodeResource(resources, R.mipmap.sym_def_app_icon)
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(
            this,
            "channel_id"
        )
            .setContentTitle(notification.title + " by service")
            .setContentText(notification.body + " by service")
            .setAutoCancel(true)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setContentIntent(pendingIntent)
            .setContentInfo(notification.title)
            .setColor(Color.RED)
            .setLights(Color.RED, 1000, 300)
            .setDefaults(Notification.DEFAULT_VIBRATE)
            .setLargeIcon(icon)
            .setSmallIcon(R.drawable.sym_def_app_icon)
        try {
            val picture_url: String? = data.get("image")
            if (picture_url != null && "" != picture_url) {
                val url = URL(picture_url)
                val bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                notificationBuilder.setStyle(
                    NotificationCompat.BigPictureStyle().bigLargeIcon(bigPicture)
                        .bigPicture(bigPicture)
                        .setSummaryText(notification.body.toString() + " by service expanding notification")
                )
            } else {
                if (notification.imageUrl != null) {
                    val url = URL(notification.imageUrl.toString())
                    val bigPicture =
                        BitmapFactory.decodeStream(url.openConnection().getInputStream())
                    notificationBuilder.setStyle(
                        NotificationCompat.BigPictureStyle().bigLargeIcon(bigPicture)
                            .bigPicture(bigPicture)
                            .setSummaryText(notification.body.toString() + " by service expanding notification")
                    )
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "channel_id", "channel_name", NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = "channel description"
            channel.setShowBadge(true)
            channel.canShowBadge()
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0, notificationBuilder.build())
    }
}

